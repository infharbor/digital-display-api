		// Method to wrap over MessageLinked
		apiCall( integer num, string func, string call ){
    		llMessageLinked(LINK_SET, num, func, call);
		}
		
        // Disconnects from normal state to make changes to configuration. 
        // (This does not errase any previously made configurations)
        apiCall(-1, "api.disconnect", NULL_KEY);

        // Need support? This call will automatically load the support URL for this product.
        apiCall(1, "get.support", NULL_KEY);

        // Verbose set to true will enable the api to llOwnerSay what is going on inside the script.
        // If Verbose is set to false the script will stay silent
        apiCall(0, "config.verbose", "true");

        // Font Families (self explanitory)
        // Default family is Arial. Arial is the only font supported at this time.
        apiCall(0, "config.font.family", "Arial");

        // Font Color 
        // Change the color of the font displayed, default is black
        // Use the vector format <R,G,B> 
        apiCall(0, "config.font.color", "<0,0,0>");

        // This is where the magic happens, you need to tell the script 
        // which links to display the character(s) on.
        // Use link.list to list out the individual links
        apiCall(0, "config.link.list", "2,3,4,5,6,7,8,9");

        // This is where the magic happens, you need to tell the script 
        // which links to display the character(s) on.
        // Use link.range to set a range from 1 to 5
        apiCall(0, "config.link.range", "2-9");

        // This completes the configuration portion and allows you to start using the API.
        apiCall(1, "api.connect", NULL_KEY);

        // Testing display with all available characters
        apiCall(2, "display.change", "abcdefghijklmnopqrstuvwxyz");
        apiCall(2, "display.change", "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        apiCall(2, "display.change", "0123456789");
        apiCall(2, "display.change", "`~!@#$%^&*()_-+={}[]|\\:;\"'<>,.?/");

        // Clears the link list/range so that it still has the texture loaded
        // but displays nothing.
        apiCall(2, "display.clear", NULL_KEY);